# Arthnor web interface

This is a web interface for [Arthnor](http://q.geek.nz/arthnor), and potentially any MOO, written in Dart. It connects to the MOO server via a WebSocket relay.

You can find the live version at http://q.geek.nz/arthnorweb/.
