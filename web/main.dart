import 'dart:convert';
import 'dart:html';
import 'dart:collection';

import 'package:smart_dialogs/smart_dialogs.dart';

class MooObject {
  String name;
  String objNum;

  MooObject(String name, String objNum) {
    this.name = name;
    this.objNum = objNum;
  }

  @override
  String toString() {
    return '${name}(${objNum})';
  }
}

void addToLog(String line) {
  var log = querySelector('#log');
  log.innerHtml += line;
  log.scrollTop = log.scrollHeight;
}

String serverToHtml(String message) {
  String escaped = HtmlEscape().convert(message);
  return processColours(escaped);
}

/// Parse the given string from an @contents command into a list of MooObjects, sorted from longest name to shortest.
List<MooObject> parseContents(String contents) {
  List<MooObject> items = [];

  // Parse contents string.
  int start = contents.indexOf('contains:\n');
  if (start != -1) {
    start += 'contains:\n'.length;
    RegExp itemRe = RegExp(r"(\w[\w '_-]*)\((#\d+)\)");
    itemRe.allMatches(contents, start).forEach((Match m) {
      items.add(MooObject(m.group(1), m.group(2)));
    });
  }

  // Sort by name length.
  items.sort((a, b) => b.name.length.compareTo(a.name.length));

  return items;
}

String addItemLinks(String description, List<MooObject> contents) {
  contents.forEach((MooObject item) {
    RegExp re = RegExp(' ${RegExp.escape(item.name)}(?=(,| and |  | x[0-9]|\n|\$))');
    description = description.replaceFirst(re, ' <a href="#${item.objNum};${HtmlEscape(HtmlEscapeMode.attribute).convert(item.name)}">${HtmlEscape().convert(item.name)}</a>');
  });
  return description;
}

/// Process a player description ready to display. This does serverToHtml, plus adds some links.
String processPlayer(String description, List<MooObject> contents) {
  String escaped;
  int inventoryStart = description.indexOf('Carrying:\n');
  if (inventoryStart != -1) {
    inventoryStart += 'Carrying:\n'.length;
    int inventoryEnd = description.indexOf('Total mass:');
    String before = description.substring(0, inventoryStart);
    String inventory = inventoryEnd == -1 ? description.substring(inventoryStart) : description.substring(inventoryStart, inventoryEnd);
    String after = inventoryEnd == -1 ? '' : description.substring(inventoryEnd);

    escaped = HtmlEscape().convert(before) + addItemLinks(inventory, contents) + HtmlEscape().convert(after);
  } else {
    escaped = HtmlEscape().convert(description);
  }
  return processColours(escaped);
}

String processRoom(String description, List<MooObject> contents) {
  RegExp itemsRe = RegExp(r"You see [\w', _-]+ here.");
  RegExp peopleRe = RegExp(r"[\w', _-]+(?= is here.| are here.)");
  String beforeItems, itemsEscaped, afterItems;
  Match itemsMatch = itemsRe.firstMatch(description);
  if (itemsMatch != null) {
    int itemsStart = itemsMatch.start + 'You see'.length;
    int itemsEnd = itemsMatch.end - ' here.'.length;
    String itemsString = description.substring(itemsStart, itemsEnd);
    beforeItems = description.substring(0, itemsStart);
    itemsEscaped = addItemLinks(itemsString, contents);
    afterItems = description.substring(itemsEnd);
  } else {
    beforeItems = '';
    itemsEscaped = '';
    afterItems = description;
  }
  String beforePeople, peopleEscaped, afterPeople;
  Match peopleMatch = peopleRe.firstMatch(afterItems);
  if (peopleMatch != null) {
    int peopleStart = peopleMatch.start;
    int peopleEnd = peopleMatch.end;
    String peopleString = afterItems.substring(peopleStart, peopleEnd);
    beforePeople = afterItems.substring(0, peopleStart);
    peopleEscaped = addItemLinks(' ' + peopleString, contents);
    afterPeople = afterItems.substring(peopleEnd);
  } else {
    beforePeople = '';
    peopleEscaped = '';
    afterPeople = afterItems;
  }
  return processColours(HtmlEscape().convert(beforeItems)
    + itemsEscaped + HtmlEscape().convert(beforePeople)
    + peopleEscaped + HtmlEscape().convert(afterPeople));
}

List<Node> processActions(String examOutput, String unambiguousName, String name, bool room) {
  RegExp objNumRe = RegExp(r' \(aka #(\d+)[, ]');
  int objNum = int.parse(objNumRe.firstMatch(examOutput).group(1));
  List<Node> out = [];
  String actionsSelector = room ? '#roomActions' : '#itemActions';

  var refresh = () {
    // Refresh item if necessary. No need to refresh room as doCommand already does that.
    if (!room) {
      showItem(unambiguousName, name);
    }
  };

  int verbsStart = examOutput.indexOf('Obvious verbs:\n');
  if (verbsStart != -1) {
    verbsStart += 'Obvious verbs:\n'.length;
    String verbs = examOutput.substring(verbsStart);

    RegExp actionRe = RegExp('^  ([\\w@/*_-]+)(?: (${unambiguousName}|<anything>))?(?: (\\w+))?(?: (${unambiguousName}|<anything>))?', multiLine: true);
    actionRe.allMatches(verbs).forEach((Match m) {
      String verb = m.group(1).replaceAll('*', '');
      String dobj = '';
      if (m.group(2) == unambiguousName) {
        dobj = 'this';
      } else if (m.group(2) == '<anything>') {
        dobj = 'any';
      }
      String prep = m.group(3) ?? '';
      String iobj = '';
      if (m.group(4) == unambiguousName) {
        iobj = 'this';
      } else if (m.group(4) == '<anything>') {
        iobj = 'any';
      }
      AnchorElement verbAnchor = AnchorElement();
      verbAnchor.text = '${verb} ${dobj} ${prep} ${iobj}';
      if (dobj != 'any' || iobj != 'any') {
        String firstVerb = verb.split('/')[0];
        verbAnchor.href = '#0';

        if (dobj == 'any') {
          // iobj must be this or none
          String iobjNum = iobj == 'this' ? '#${objNum}' : '';
          String iobjName = iobj == 'this' ? name : '';
          verbAnchor.onClick.listen((MouseEvent e) async {
            String dobjSelected = (await Info.get(
              null, 'What do you want to ${firstVerb} ${prep} ${iobjName}?', [''], null, [null], [20], [null], false, null
              )).getUserInput(0)[0];
            // Hide actions so the user doesn't click again before we're ready.
            querySelector(actionsSelector).innerHtml = '';
            String command = '${firstVerb} ${dobjSelected} ${prep} ${iobjNum}';
            doCommand(command, refresh);
            querySelector('#input').focus();
          });
        } else if (iobj == 'any') {
          // doj must be this or none
          String dobjNum = dobj == 'this' ? '#${objNum}' : '';
          String dobjName = dobj == 'this' ? name : '';
          verbAnchor.onClick.listen((MouseEvent e) async {
            String iobjSelected = (await Info.get(
              null, 'What do you want to ${firstVerb} ${dobjName} ${prep}?', [''], null, [null], [20], [null], false, null
              )).getUserInput(0)[0];
            // Hide actions so the user doesn't click again before we're ready.
            querySelector(actionsSelector).innerHtml = '';
            String command = '${firstVerb} ${dobjNum} ${prep} ${iobjSelected}';
            doCommand(command, refresh);
            querySelector('#input').focus();
          });
        } else {
          // Both objects are this or none
          String dobjNum = dobj == 'this' ? '#${objNum}' : '';
          String iobjNum = iobj == 'this' ? '#${objNum}' : '';
          String command = '${firstVerb} ${dobjNum} ${prep} ${iobjNum}';
          verbAnchor.onClick.listen((MouseEvent e) {
            // Hide actions so the user doesn't click again before we're ready.
            querySelector(actionsSelector).innerHtml = '';
            doCommand(command, refresh);
            querySelector('#input').focus();
          });
        }
      }
      out.add(verbAnchor);
      out.add(BRElement());
    });
  }

  return out;
}

String processColours(String line) {
  String output = '';
  RegExp re = RegExp(r'~([a-z-]+);');
  int cursor = 0;
  List<String> stack = [];

  void open(String tag, [String params = '']) {
    stack.add(tag);
    output += '<${tag} ${params}>';
  }
  void close(String tag) {
    if (stack.contains(tag)) {
      // Close any other higher level open tags first.
      bool done = false;
      while (stack.isNotEmpty && !done) {
        done = stack.last == tag;
        output += '</${stack.removeLast()}>';
      }
    }
  }
  void closeAll([String tag]) {
    while (stack.isNotEmpty && (tag == null || stack.last == tag)) {
      output += '</${stack.removeLast()}>';
    }
  }

  re.allMatches(line).forEach((Match m) {
    output += line.substring(cursor, m.start);
    String code = line.substring(m.start + 1, m.end - 1);
    switch (code) {
      case 'title':
        open('h2');
      break;
      case '-title':
        close('h2');
      break;
      case 'bold':
        open('b');
      break;
      case '-bold':
        close('b');
      break;
      case 'italic':
        open('i');
      break;
      case '-italic':
        close('i');
      break;
      case 'ul':
        open('ul');
      break;
      case '-ul':
        close('ul');
      break;
      case 'pre':
        open('pre');
      break;
      case '-pre':
        close('pre');
      break;
      case 'black':
      case 'red':
      case 'green':
      case 'brown':
      case 'blue':
      case 'magenta':
      case 'cyan':
      case 'white':
      case 'bblack':
      case 'bred':
      case 'bgreen':
      case 'bbrown':
      case 'bblue':
      case 'bmagenta':
      case 'bcyan':
      case 'bwhite':
        open('span', 'class="${code}"');
      break;
      case 'defcol':
      case 'bdefcol':
        closeAll('span');
      break;
      default:
        output += '~' + code + ';';
      break;
    }
    cursor = m.end;
  });
  output += line.substring(cursor);
  closeAll();

  return output.replaceAll('</h2>\n', '</h2>').replaceAll('</pre>\n', '</pre>').replaceAll('\n', '<br/>');
}

void showItem(String unambiguousName, String name) {
  startCapture('l ${unambiguousName}', (String result) {
    String itemHtml = serverToHtml(result);
    if (!itemHtml.contains('<h2')) {
      itemHtml = '<h2>${name}</h2>' + itemHtml;
    }
    querySelector('#itemHide').hidden = false;
    querySelector('#item').innerHtml = itemHtml;
    querySelector('#itemActions').innerHtml = '';
    startCapture('exam ${unambiguousName}', (String result) {
      querySelector('#itemActions').nodes = processActions(result, unambiguousName, name, false);
    });
  });
}

void hideItem() {
    querySelector('#itemHide').hidden = true;
    querySelector('#item').innerHtml = '';
    querySelector('#itemActions').innerHtml = '';
}

void move(String direction) {
  startCapture(direction, (String result) {
    addToLog(serverToHtml(result));
    querySelector('#roomActions').innerHtml = '';
    updateRoom();
  });
}

AnchorElement makeExitLink(String direction) {
  AnchorElement exit = AnchorElement(href: '#0');
  exit.text = direction;
  exit.onClick.listen((MouseEvent e) {
    move(direction);
    querySelector('#input').focus();
  });
  return exit;
}

List<Node> processExits(String exits) {
  List<Node> out = [];
  RegExp re = RegExp(r'(\w+) \([\w ]+\)');
  var matches = List.of(re.allMatches(exits).map((RegExpMatch m) => m.group(1)));

  // Organise cardinal directions into a table.
  const directions = [['northwest', 'north', 'northeast'], ['west', 'in', 'east'], ['southwest', 'south', 'southeast']];
  var table = TableElement();
  directions.forEach((var row) {
    var tr = table.addRow();
    row.forEach((var direction) {
      var cell = tr.addCell();
      cell.classes.add('exit');
      if (matches.contains(direction)) {
        matches.remove(direction);
        cell.children = [makeExitLink(direction)];
      }
    });
  });
  out.add(table);

  // Show remaining exits in a list.
  matches.forEach((String direction) {
    out.add(makeExitLink(direction));
    out.add(BRElement());
  });
  return out;
}

const String EOC = '#done#';

var ws;
Function(String) captureHandler;
Function(String) snooper;
String captureResult;

void startCapture(String command, Function(String) handler) {
  captureResult = '';
  captureHandler = handler;
  ws.send('${command}\n@echo ${EOC}\n');
}

void updatePlayer([Function() next]) {
  startCapture('l me', (String description) {
    startCapture('@contents me', (String contents) {
      querySelector('#player').innerHtml = processPlayer(description, parseContents(contents));
      if (next != null) {
        next();
      }
    });
  });
}

void updateRoom([Function() next]) {
  startCapture('l', (String description) {
    startCapture('@contents here', (String contents) {
      querySelector('#room').innerHtml = processRoom(description, parseContents(contents));
      startCapture('@ways', (String result) {
        querySelector('#exits').nodes = processExits(result);
        startCapture('exam here', (String result) {
          querySelector('#roomActions').nodes = processActions(result, 'here', 'here', true);
          if (next != null) {
            next();
          }
        });
      });
    });
  });
}

void login(String username, String password, bool register, Function() onSuccess) {
  snooper = (String line) {
    if (line.contains('***')) {
      snooper = null;
      onSuccess();
    }
  };
  var command = register ? 'cr' : 'co';
  ws.send('${command} ${username} ${password}\n');
}

void main() {
  var loginBox = querySelector('#loginBox');
  var loginForm = querySelector('#loginForm');
  var usernameField = querySelector('#username') as InputElement;
  var passwordField = querySelector('#password') as InputElement;
  var loginButton = querySelector('#loginButton') as InputElement;
  var registerButton = querySelector('#registerButton') as InputElement;
  var inputBox = querySelector('#inputBox');
  var inputForm = querySelector('#inputForm');
  var inputField = querySelector('#input') as TextInputElement;
  var itemHide = querySelector('#itemHide');

  // The 0th element of the scrollback represents the command currently being entered; the rest are
  // previously submitted commands.
  var scrollback = [''];
  var scrollbackPosition = 0;

  addToLog('Started<br/>');
  ws = WebSocket('wss://q.geek.nz/arthnorws/');
  ws.onMessage.listen((MessageEvent e) {
    var line = e.data;
    if (captureHandler != null) {
      if (line.trim() == EOC) {
        var handler = captureHandler;
        captureHandler = null;
        handler(captureResult);
      } else {
        captureResult += line;
      }
    } else {
      addToLog(serverToHtml(line));
    }
    if (snooper != null) {
      snooper(line);
    }
  });
  ws.onOpen.listen((Event e) {
    if (ws != null && ws.readyState == WebSocket.OPEN) {
      addToLog('Opened<br/>');
      loginButton.disabled = false;
      registerButton.disabled = false;
      usernameField.focus();
    } else {
      addToLog('WebSocket not connected, message not sent<br/>');
    }
  });
  ws.onClose.listen((CloseEvent e) {
    loginButton.disabled = true;
    registerButton.disabled = true;
    inputBox.hidden = true;
    addToLog('Connection closed<br/>');
  });

  loginForm.onSubmit.listen((Event e) async {
    e.preventDefault();
    if (usernameField.value.isEmpty) {
      usernameField.focus();
    } else if (passwordField.value.isEmpty) {
      passwordField.focus();
    } else if (ws != null && ws.readyState == WebSocket.OPEN) {
      bool register = document.activeElement == registerButton;
      if (register) {
        UserInput result = await Info.confirm('Are you sure you want to create a new character called ${usernameField.value}?', ['Yes', 'No']);
        if (result.buttonCode != DiaAttr.DIA_ACT_BUT1) {
          return;
        }
      }
      login(usernameField.value, passwordField.value, register, () {
        loginBox.hidden = true;
        inputBox.hidden = false;
        inputField.focus();
        startCapture('@colour codes', (String result) {
          updatePlayer(() {
            updateRoom();
          });
        });
      });
    }
  });

  inputField.onKeyDown.listen((KeyboardEvent e) {
    KeyEvent ke = KeyEvent.wrap(e);
    if (ke.keyCode == KeyCode.UP) {
      e.preventDefault();
      if (scrollbackPosition == 0) {
        // The 0th element of the scrollback is the command that hasn't been submitted yet, so we
        // can save it in its current state.
        scrollback.first = inputField.value;
      }
      if (scrollbackPosition < scrollback.length - 1) {
        ++scrollbackPosition;
        inputField.value = scrollback.elementAt(scrollbackPosition);
      }
    } else if (ke.keyCode == KeyCode.DOWN) {
      e.preventDefault();
      if (scrollbackPosition > 0) {
        --scrollbackPosition;
        inputField.value = scrollback.elementAt(scrollbackPosition);
      }
    }
  });

  inputForm.onSubmit.listen((Event e) {
    e.preventDefault();
    if (inputField.value == '') {
      updatePlayer();
    } else {
      var command = inputField.value;
      if (scrollback.length < 2 || scrollback[1] != command) {
        scrollback.insert(1, command);
      }
      scrollback.first = '';
      scrollbackPosition = 0;
      doCommand(command);
    }
    inputField.value = '';
  });

  itemHide.onClick.listen((MouseEvent e) {
    hideItem();
    inputField.focus();
  });

  window.onHashChange.listen((Event e) {
    HashChangeEvent he = e as HashChangeEvent;
    Uri url = Uri.parse(he.newUrl);
    String fragment = Uri.decodeComponent(url.fragment);
    if (fragment.length > 1) {
      List<String> parts = fragment.split(';');
      if (parts.length == 1) {
        showItem('1st ${parts[0]}', parts[0]);
      } else {
        showItem(parts[0], parts[1]);
      }
    }
    inputField.focus();
  });
}

void doCommand(String command, [Function() next]) {
  addToLog('>' + serverToHtml(command) + '<br/>');
  startCapture(command, (String result) {
    addToLog('<div class="response">' + serverToHtml(result) + '</div>');
    updatePlayer(() => updateRoom(next));
  });
}
